﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Connectables
{
    /// <summary>
    /// Составная часть Connectable для работы со всеми соединениями.
    /// </summary>
    public class Connector : MonoBehaviour
    {
        MeshRenderer _renderer;

        List<Line> _lines;

        public void Init()
        {
            _renderer = gameObject.GetComponent<MeshRenderer>();
            _lines = new List<Line>();
        }

        public Vector3 Position => transform.position;

        public void Select() => ChangeColor(Color.yellow);
        public void Highlight() => ChangeColor(Color.blue);

        public void Unselect() => ChangeColor(Color.white);

        private void ChangeColor(Color color)
        {
            _renderer.material.color = color;
        }

        public void AddLink(Line line) => _lines.Add(line);
    }
}
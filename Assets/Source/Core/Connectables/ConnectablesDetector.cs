﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Connectables
{
    /// <summary>
    /// Утилита для обнаружения составных частей Connectable.
    /// </summary>
    public class ConnectablesDetector
    {
        private Camera _camera;

        public ConnectablesDetector(Camera camera) => _camera = camera;


        /// <summary>
        /// Возвращает составные части, обнаруженные в позиции курсора.
        /// </summary>
        /// <param name="mousePosition"></param>
        /// <returns></returns>
        public (Connector connector, MovingPlatform platform) 
            DetectConnectableComponents(Vector3 mousePosition)
        {
            var ray = _camera.ScreenPointToRay(mousePosition);

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                var collider = hit.collider;

                var connector = collider.GetComponent<Connector>();
                var platform = collider.GetComponent<MovingPlatform>();

                return (connector, platform);
            }

            return (null, null);
        }

        /// <summary>
        /// Возращает только Connector, если обнаруживается Connectable.
        /// </summary>
        /// <param name="mousePosition"></param>
        /// <returns></returns>
        public Connector DetectConnector(Vector3 mousePosition)
        {
            var ray = _camera.ScreenPointToRay(mousePosition);

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                var collider = hit.collider;

                var connector = collider.GetComponent<Connector>();
                return (connector);
            }

            return null;
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Connectables 
{
    /// <summary>
    /// MonoBehaviour для настройки соединения между Connectable.
    /// </summary>
    [RequireComponent(typeof(LineRenderer))]
    public class Line : MonoBehaviour
    {
        LineRenderer _renderer;

        Connector _source;
        Connector _target;

        private void Init(Connector source)
        {
            _renderer = gameObject.GetComponent<LineRenderer>();
            _source = source;
        }

        private void Init(Connector sourse, Vector3 targetPosition)
        {
            Init(sourse);
            TargetPosition = targetPosition;
        }

        private void Init(Connector source, Connector target)
        {
            Init(source);
            _target = target;
        }

        public Vector3 TargetPosition { get; set; }

        private void Update()
        {
            _renderer.SetPosition(0, _source.Position);

            if (_target != null) _renderer.SetPosition(1, _target.Position);
            else _renderer.SetPosition(1, TargetPosition);
        }

        public void SetTarget(Connector target) => _target = target;

        public class Factory
        {
            Transform _group;
            Line _prefab;

            public Factory(Line prefab)
            {
                _prefab = prefab;
                _group = new GameObject("Lines").transform;
            }

            public Line Create(Connector source, Vector3 targetPosition)
            {
                var line = GameObject.Instantiate<Line>(_prefab, _group);
                line.Init(source, targetPosition);

                return line;
            }

            public Line Create(Connector source, Connector target)
            {
                var line = GameObject.Instantiate<Line>(_prefab, _group);
                line.Init(source, target);

                return line;
            }
        }
    }
}
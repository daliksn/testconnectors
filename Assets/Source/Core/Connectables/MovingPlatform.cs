﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Connectables
{
    /// <summary>
    /// MonoBehaviour для работы с передвижением Connectable.
    /// </summary>
    public class MovingPlatform : MonoBehaviour
    {
        private Transform _movingTarget;

        public void Init() => _movingTarget = transform.parent;

        public Vector3 Position
        {
            get => _movingTarget.position;
            set => _movingTarget.position = value;
        }
    }
}
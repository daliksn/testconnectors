﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Простая визуализация зоны действия Main.
/// </summary>
[RequireComponent(typeof(LineRenderer))]
[RequireComponent(typeof(Main))]
public class MainZoneDrawer : MonoBehaviour
{
    [Range(0, 50), SerializeField] int countSegments = 50;

    Main _main;
    LineRenderer _line;

    private void Awake()
    {
        _line = gameObject.GetComponent<LineRenderer>();
        _main = gameObject.GetComponent<Main>();

        DrawCircle();
    }

    private void DrawCircle()
    {
        _line.positionCount = countSegments + 1;

        var angle = 20f;
        for (int i = 0; i <= countSegments; i++)
        {
            var x = Mathf.Sin(Mathf.Deg2Rad * angle) * _main.Radius;
            var z = Mathf.Cos(Mathf.Deg2Rad * angle) * _main.Radius;

            _line.SetPosition(i, new Vector3(x, 0, z));

            angle += (360f / countSegments);
        }
    }
}
    
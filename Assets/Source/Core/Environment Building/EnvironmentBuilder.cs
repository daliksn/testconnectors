﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Connectables;

namespace EnvironmentBuilding
{
    /// <summary>
    /// Строит необходимое для работы окружение. 
    /// Создает указанное количество Connectable в пределах зоны Main
    /// и возвращает их коллекцию.
    /// </summary>
    public class EnvironmentBuilder
    {
        Main _main;
        Connectable.Factory _factory;

        int countConnectables;

        public EnvironmentBuilder(Main main, int countConnectables, Connectable.Factory factory)
        {
            _main = main;
            _factory = factory;

            this.countConnectables = countConnectables;
        }

        public IReadOnlyCollection<Connectable> Create()
        {
            var connectables = new List<Connectable>();

            for (int i = 0; i < countConnectables; i++)
            {
                var position = GetRandomPosition();
                var angle = GetRandomAngle();

                var connectable = _factory.Create(position, Quaternion.Euler(0f, angle, 0f));
                connectables.Add(connectable);
            }

            return connectables;
        }

        private Vector3 GetRandomPosition()
        {
            var angle = GetRandomAngle();
            var distance = Random.Range(0f, _main.Radius);

            var x = Mathf.Sin(Mathf.Deg2Rad * angle) * distance;
            var z = Mathf.Cos(Mathf.Deg2Rad * angle) * distance;

            var position = _main.transform.position + new Vector3(x, 0, z);

            return position;
        }

        private float GetRandomAngle() => Random.Range(0f, 360f);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CameraBehaviour;
using Connectables;
using EnvironmentBuilding;
using EnvironmentSystems;

namespace Startuping
{
    /// <summary>
    /// Точка входа в сцену.
    /// Хранит необходимые ресурсы и параметры 
    /// и на их основе строит системные модули для приложения.
    /// </summary>
    public class Startup : MonoBehaviour
    {
        [SerializeField] Connectable _prefabConnectable;
        [SerializeField] Line _prefabLine;

        [SerializeField] int countConnectables = 10;

        private void Awake()
        {
            var main = Object.FindObjectOfType<Main>();
            var flightCamera = Object.FindObjectOfType<FlightCamera>();

            var cFactory = new Connectable.Factory(_prefabConnectable);
            var lFactory = new Line.Factory(_prefabLine);

            var builder = new EnvironmentBuilder(main, countConnectables, cFactory);
            var connectables = builder.Create();

            var controller = new EnvironmentController(flightCamera, connectables, lFactory);
            controller.TransferControl();
        }
    }
}

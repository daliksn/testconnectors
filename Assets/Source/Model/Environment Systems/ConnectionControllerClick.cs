﻿using UnityEngine;
using Cysharp.Threading.Tasks;
using Connectables;

namespace EnvironmentSystems
{
    /// <summary>
    /// Контроллер управления мгновенного соединения.
    /// </summary>
    public class ConnectionControllerClick : ISubsystemController<Connector>
    {
        Line.Factory _factory;

        public ConnectionControllerClick(Line.Factory factory)
        {
            _factory = factory;
        }

        public async UniTask TransferControl(Connector connector)
        {
            var detector = new ConnectablesDetector(Camera.main);

            while (true)
            {
                await UniTask.Yield();

                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    var target = detector.DetectConnector(Input.mousePosition);
                    if (target != null && target != connector)
                    {
                        var line = _factory.Create(connector, target);
                        connector.AddLink(line);
                    }

                    break;
                }
            }
        }
    }
}
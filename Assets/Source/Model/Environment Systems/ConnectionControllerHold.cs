﻿using UnityEngine;
using Cysharp.Threading.Tasks;
using Connectables;

namespace EnvironmentSystems
{
    /// <summary>
    /// Контроллер управления удерживающего соединения.
    /// </summary>
    public class ConnectionControllerHold : ISubsystemController<Connector>
    {
        Line.Factory _factory;

        public ConnectionControllerHold(Line.Factory factory) => _factory = factory;

        public async UniTask TransferControl(Connector connector)
        {
            Cursor.visible = false;

            var camera = Camera.main;
            var detector = new ConnectablesDetector(camera);

            var mousePosition = Input.mousePosition;
            var distenceToScreen = camera.WorldToScreenPoint(connector.Position).z;
            var position = camera.ScreenToWorldPoint(
                new Vector3(mousePosition.x, mousePosition.y, distenceToScreen));

            var line = _factory.Create(connector, position);

            Connector foundConnector = null;

            while (true)
            {
                await UniTask.Yield();
                if (line == null || connector == null) break;

                if (Input.GetKeyUp(KeyCode.Mouse0))
                {
                    if (foundConnector != null)
                    {
                        line.SetTarget(foundConnector);
                        connector.AddLink(line);
                    }
                    else
                    {
                        GameObject.Destroy(line.gameObject);
                    }

                    break;
                }

                mousePosition = Input.mousePosition;
                distenceToScreen = camera.WorldToScreenPoint(line.TargetPosition).z;

                var newPosition = camera.ScreenToWorldPoint(
                    new Vector3(mousePosition.x, mousePosition.y, distenceToScreen));
                newPosition.y = position.y;

                line.TargetPosition = newPosition;

                var target = detector.DetectConnector(mousePosition);
                if (target == null && foundConnector != null)
                {
                    foundConnector.Highlight();
                    foundConnector = null;
                }
                else if (target != null && target != connector)
                {
                    if (target != foundConnector)
                    {
                        foundConnector?.Highlight();
                        foundConnector = target;
                        foundConnector.Select();
                    }
                }
            }

            Cursor.visible = true;
        }
    }
}
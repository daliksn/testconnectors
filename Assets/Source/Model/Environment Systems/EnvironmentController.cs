﻿using System.Runtime.InteropServices;
using System.Collections.Generic;
using UnityEngine;
using Cysharp.Threading.Tasks;
using CameraBehaviour;
using Connectables;

namespace EnvironmentSystems
{
    /// <summary>
    /// Контроллер, обновляющий окружение в зависимости от типа ввода.
    /// </summary>
    public class EnvironmentController
    {
        ISubsystemController _cameraController;
        ISubsystemController _connectablesController;

        public EnvironmentController(FlightCamera camera, 
            IReadOnlyCollection<Connectable> connectables, Line.Factory factory)
        {
            _cameraController = new CameraController(camera);
            _connectablesController = new ConnectablesController(connectables, factory);
        }

        /// <summary>
        /// Передает полное управление окружением.
        /// Асинхронный поток принимает на вход тип кнопки мышки
        /// и передает управление окружением ответственному за задачу контроллеру,
        /// ожидая его завершение.
        /// </summary>
        public async void TransferControl()
        {
            while (true)
            {
                await UniTask.Yield();

                if (Input.GetKeyDown(KeyCode.Mouse1)) await _cameraController.TransferControl();
                else if (Input.GetKeyDown(KeyCode.Mouse0))
                    await _connectablesController.TransferControl();
            }
        }
    }

    static class CursorUtility
    {
        [DllImport("user32.dll")]
        public static extern bool SetCursorPos(int x, int y);

        [DllImport("user32.dll")]
        public static extern bool GetCursorPos(out MousePosition position);

        [StructLayout(LayoutKind.Sequential)]
        public struct MousePosition
        {
            public int x;
            public int y;
        }
    }
}
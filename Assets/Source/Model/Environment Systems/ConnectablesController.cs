﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cysharp.Threading.Tasks;
using Connectables;

namespace EnvironmentSystems
{
    /// <summary>
    /// Контроллер управления Connectables.
    /// В зависимости от ввода использует вложенные контроллеры подсистем 
    /// для перемещания и соединения коннекторов.
    /// </summary>
    public class ConnectablesController : ISubsystemController
    {
        ISubsystemController<MovingPlatform> _movingPlatformController;
        ISubsystemController<Connector> _connectionControllerClick;
        ISubsystemController<Connector> _connectionControllerHold;

        IReadOnlyCollection<Connectable> _connectables;

        ConnectablesDetector _detector;

        public ConnectablesController(IReadOnlyCollection<Connectable> connectables, Line.Factory factory)
        {
            _connectables = connectables;

            _movingPlatformController = new MovingPlatformController();
            _connectionControllerClick = new ConnectionControllerClick(factory);
            _connectionControllerHold = new ConnectionControllerHold(factory);

            _detector = new ConnectablesDetector(Camera.main);
        }

        public async UniTask TransferControl()
        {
            var result = _detector.DetectConnectableComponents(Input.mousePosition);

            if (result.platform != null) await _movingPlatformController.TransferControl(result.platform);
            else if (result.connector != null)
            {
                var connector = result.connector;

                foreach (var connectable in _connectables)
                {
                    var current = connectable.Connector;

                    if (connector == current) connector.Select();
                    else current.Highlight();
                }

                await UniTask.Delay(150); //Если после микрозадержки кнопка не отпустилась, то пользователь ожидает удерживающее, а не мгновенное соединение.

                if (Input.GetKey(KeyCode.Mouse0)) await _connectionControllerHold.TransferControl(connector);
                else await _connectionControllerClick.TransferControl(connector);

                foreach (var connectable in _connectables)
                {
                    var current = connectable.Connector;
                    current.Unselect();
                }
            }
                
        }
    }
}
﻿using UnityEngine;
using Cysharp.Threading.Tasks;
using Connectables;

namespace EnvironmentSystems
{
    /// <summary>
    /// Контроллер управления движением Connectable.
    /// </summary>
    public class MovingPlatformController : ISubsystemController<MovingPlatform>
    {
        public async UniTask TransferControl(MovingPlatform platform)
        {
            Cursor.visible = false;

            var camera = Camera.main;

            var mousePosition = Input.mousePosition;
            var distenceToScreen = camera.WorldToScreenPoint(platform.Position).z;

            var offset = platform.Position - camera.ScreenToWorldPoint(new Vector3(mousePosition.x, mousePosition.y, distenceToScreen));

            while (true)
            {
                await UniTask.Yield();
                if (platform == null) break;
                if (Input.GetKeyUp(KeyCode.Mouse0)) break;

                mousePosition = Input.mousePosition;
                distenceToScreen = camera.WorldToScreenPoint(platform.Position).z;

                var newPosition = camera.ScreenToWorldPoint(new Vector3(mousePosition.x, mousePosition.y, distenceToScreen));
                newPosition += offset;
                newPosition.y = platform.Position.y;

                platform.Position = newPosition;
            }

            Cursor.visible = true;
        }
    }
}
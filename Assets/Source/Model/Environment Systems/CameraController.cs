﻿using UnityEngine;
using Cysharp.Threading.Tasks;
using CameraBehaviour;

namespace EnvironmentSystems
{
    /// <summary>
    /// Контроллер управления свободного полета камеры.
    /// Включает свободный полет камеры и выключает режим
    /// при отпуске правой кнопки мышки.
    /// </summary>
    class CameraController : ISubsystemController
    {
        FlightCamera _camera;

        public CameraController(FlightCamera camera) => _camera = camera;

        public async UniTask TransferControl()
        {
            if (Input.GetKeyUp(KeyCode.Mouse1)) return;

            CursorUtility.MousePosition position;
            CursorUtility.GetCursorPos(out position);

            Cursor.lockState = CursorLockMode.Locked;
            _camera.EnableFlightMode();

            while (true)
            {
                await UniTask.Yield();

                if (Input.GetKeyUp(KeyCode.Mouse1))
                {
                    _camera.DisableFlightMode();
                    Cursor.lockState = CursorLockMode.None;
                    CursorUtility.SetCursorPos(position.x, position.y);
                    break;
                }
            }
        }

      
    }
}